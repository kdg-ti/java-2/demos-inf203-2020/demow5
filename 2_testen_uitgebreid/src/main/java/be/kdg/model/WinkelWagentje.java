package be.kdg.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class WinkelWagentje {
    private List<Product> wagentje;
    private double saldo;

    public WinkelWagentje() {
        wagentje = new ArrayList<>();
    }

    public void voegToe(Product product) {
        wagentje.add(product);
        saldo += product.getPrijs();
    }

    public void verwijder(Product product) throws IllegalArgumentException {
        if (!wagentje.remove(product)) {
           throw new IllegalArgumentException("Product " + product.getNaam() + " niet gevonden!");
        }
        saldo -= product.getPrijs();
    }

    public int getAantal() {
        return wagentje.size();
    }

    public double getSaldo() {
        return saldo;
    }

    public void maakWagentjeLeeg() {
        wagentje.clear();
        saldo = 0.0;
    }

    public List<Product> getProductenList() {
        return Collections.unmodifiableList(wagentje);
    }
}

